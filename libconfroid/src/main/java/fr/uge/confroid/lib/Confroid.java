package fr.uge.confroid.lib;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;

import androidx.core.app.JobIntentService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

import fr.uge.confroid.lib.annotation.AnotationReader;

public final class Confroid<T> {

    private static final AtomicInteger ID_GENERATOR = new AtomicInteger();

    private final Class<T> type;
    private final Context context;
    private final String name;

    // fonction de callback
    private  Consumer callback;

    Gson gson = new GsonBuilder()
            .registerTypeAdapterFactory(new BundleTypeAdapterFactory())
            .create();



    public Confroid(Class<T> type, Context context, String name) {
        this.type = type;
        this.context = context;
        this.name = name;
        context.registerReceiver(broadcastReceiver,new IntentFilter("/WRONG_TOKEN"));
        context.registerReceiver(broadcastReceiver,new IntentFilter("/UNKNOW_CONFIGURATION"));
        context.registerReceiver(broadcastReceiver,new IntentFilter("/UNKNOW_VERSION"));
        context.registerReceiver(broadcastReceiver,new IntentFilter("/SUCCESS"));

    }

    public void saveConfiguration(Context context, String name, Object value, String versionName, Class serviceClass, String token, Consumer<T> callback) {

        Bundle tmp = new Bundle();
        ObjectToBundle.fillBundle(tmp, name, value);

        Bundle b = new Bundle();
        b.putBundle("content",tmp);
        b.putString("name", context.getPackageName() + '.' + this.name);
        b.putString("prefix", context.getPackageName());


        Intent i = new Intent();
        i.putExtra("config", gson.toJson(b));
        i.putExtra("token",token);
        i.putExtra("tag",versionName);

        JobIntentService.enqueueWork(context, serviceClass,24999,i);
        this.callback = callback;
    }

    public void registerObjects(Context context, Class serviceClass, String token, List subscribedObjects) {
        Bundle registeredObjects = new Bundle();
        try {
            AnotationReader.registerObjects(registeredObjects,subscribedObjects);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        System.out.println("Registered Objects" +registeredObjects);


        Intent i = new Intent();
        i.putExtra("registeredObjects",gson.toJson(registeredObjects));
        i.putExtra("token", token);
        i.putExtra("prefix", context.getPackageName());
        JobIntentService.enqueueWork(context, serviceClass,25002,i);
        this.callback = callback;
    }



        public void loadConfiguration(Context context,String name,String version, Consumer<T> callback, Class serviceClass, String token) {
        Intent i = new Intent();
        i.putExtra("requestId",new Random().nextInt());


        i.putExtra("name", context.getPackageName() + '.' + name);
        i.putExtra("prefix", context.getPackageName());

        i.putExtra("token", token);
        i.putExtra("version",version);
        i.putExtra("receiver", context.getClass().toString());
        i.putExtra("expiration",60);
        context.registerReceiver(broadcastReceiver,new IntentFilter("/PULL_CONFIG"));

        JobIntentService.enqueueWork(context, serviceClass,25000,i);

        this.callback = callback;


    }


    public void subscribeConfiguration(Context context,Consumer<T> callback, Class serviceClass) {
        Intent i = new Intent();
        i.putExtra("prefix", context.getPackageName());
        i.putExtra("receiver", context.getClass().toString());
        JobIntentService.enqueueWork(context,serviceClass,25003, i);
        context.registerReceiver(broadcastReceiver,new IntentFilter("/GET_TOKEN"));
        this.callback = callback;
    }

    public void getConfigurationVersions(Context context, String name,Consumer<T> callback, Class serviceClass, String token) {
        Intent i = new Intent();
        i.putExtra("requestId",new Random().nextInt());

        i.putExtra("name", context.getPackageName() + '.' + name);
        i.putExtra("prefix", context.getPackageName());

        i.putExtra("token", token);

        i.putExtra("receiver", context.getClass().toString());
        i.putExtra("expiration",60);
        JobIntentService.enqueueWork(context, serviceClass,25001,i);
        context.registerReceiver(broadcastReceiver,new IntentFilter("/GET_ALL_VERSIONS"));

        this.callback = callback;
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        private final Gson gson = new GsonBuilder()
                .registerTypeAdapterFactory(new BundleTypeAdapterFactory())
                .create();
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if(action != null) {
                if(action.equals("/PULL_CONFIG")) {
                    if (intent.getExtras() != null)
                    {
                        Bundle b = intent.getBundleExtra("content");
                        int status = intent.getExtras().getInt("status");
                        Log.i("Status", String.valueOf(status));
                        Log.d("RESULT PULLE CONFIG", gson.toJson(b));
                        callback.accept(gson.toJson(b));
                    }
                } else if (action.equals("/GET_ALL_VERSIONS")) {
                    Bundle b2 = intent.getBundleExtra("versions");
                    Log.d("RESULT VERSIONS", gson.toJson(b2));
                    callback.accept(gson.toJson(b2));

                }else if (action.equals("/GET_TOKEN")) {
                    String token = intent.getStringExtra("token");
                    Log.d("TOKEN RECEIVED", token);
                    callback.accept(token);

                }
                else if (action.equals("/WRONG_TOKEN")) {
                    Log.d("AuthenTIFICATION FAILED", "fail");
                    callback.accept("Authentification Failed");
                }else if (action.equals("/UNKNOW_CONFIGURATION")) {
                    Log.d("unknow", "fail");
                    callback.accept("Unknow Configuration");
                }else if (action.equals("/UNKNOW_VERSION")) {
                    Log.d("unknow", "fail");
                    callback.accept("unknow Version");
                }else if (action.equals("/SUCCESS")) {
                    Log.d("succes", "saved succesfully");
                    callback.accept("saved succesfully");
                }

            }


        }
    };



}
