package fr.uge.confroid.lib;

import android.os.Bundle;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public final class ObjectToBundle {


    public static void fillBundle(Bundle b, String key, Object obj) {
        Map<Integer, Object> alreadyVisited = new HashMap<>();
        try {
            fillRecursive(b, key, alreadyVisited, obj);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    private static void fillRecursive(Bundle b, String key, Map<Integer, Object> alreadyVisited, Object obj) throws IllegalAccessException {
        if (obj instanceof String) {
            b.putString(key, (String) obj);
        } else if (obj instanceof Integer) {
            b.putInt(key, (int) obj);
        } else if (obj instanceof Long) {
            b.putLong(key, (long) obj);
        } else if (obj instanceof Boolean) {
            b.putBoolean(key, (boolean) obj);
        } else if (obj instanceof byte[]) {
            b.putByteArray(key, (byte[]) obj);
        } else if (obj instanceof Float) {
            b.putFloat(key, (float) obj);
        } else if (obj instanceof Double) {
            b.putDouble(key, (double) obj);
        } else if (obj instanceof Character) {
            b.putChar(key, (char) obj);
        }else if (obj instanceof float[]) {
            b.putString(key, Arrays.toString((float[]) obj));
        }else if (obj instanceof int[]) {
            b.putString(key, Arrays.toString((int[]) obj));
        }
        else if (obj instanceof ArrayList) {
            int cpt =0;
            Bundle tmp = new Bundle();
            for(Object o: (ArrayList)obj){
                fillRecursive(tmp,String.valueOf(cpt), alreadyVisited,((ArrayList) obj).get(cpt));
                cpt +=1;
            }
            b.putBundle(key,tmp);


        }else if (obj instanceof HashMap) {
            Bundle result = new Bundle();
            Bundle control = new Bundle();
            Bundle tmp = new Bundle();
            for(Object kez: ((HashMap) obj).keySet()){
                fillRecursive(tmp,kez.toString(),alreadyVisited,((HashMap) obj).get(kez));
                control.putBundle(kez.toString(),tmp);
            }
            b.putBundle(key,tmp);


        }
        else if (obj instanceof Byte) {
            b.putByte(key, (byte) obj);
        } else if (obj instanceof Short) {
            b.putShort(key, (short) obj);
        } else if (obj != null) {
            Bundle sub = new Bundle();
            int refId = -1;
            for (Map.Entry<Integer, Object> entry : alreadyVisited.entrySet()) {
                if (entry.getValue() == obj) {
                    refId = entry.getKey();
                    break;
                }
            }
            if (refId == -1) {
                int id = alreadyVisited.size() + 1;
                //identifier +=1;
                sub.putInt("id", id);
                //System.out.println(id);
                sub.putString("class", obj.getClass().getName());
                Field[] fields = obj.getClass().getDeclaredFields();
                for (Field field : fields) {
                    if (Modifier.isTransient(field.getModifiers())
                            || Modifier.isFinal(field.getModifiers())
                            || Modifier.isStatic(field.getModifiers())) {
                        continue;
                    }
                    field.setAccessible(true);
                    String name = field.getName();
                    Object value = field.get(obj);
                    fillRecursive(sub, name, alreadyVisited, value);
                }
                //System.out.println(alreadyVisited.size());
                alreadyVisited.put(id, obj);
            } else {
                sub.putInt("ref", refId);
            }
            b.putBundle(key, sub);
        }

    }
}
