package fr.uge.confroid.lib.annotation;

import android.os.Bundle;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.uge.confroid.lib.BundleTypeAdapterFactory;


// READ ANOTATIONS FROM OBJECT AND STORE THEM IN A BUNDLE
public class AnotationReader {

    public static Bundle readConstraints(Object obj) throws IllegalAccessException {
        Field[] fields = null;
        fields = obj.getClass().getFields();

        Bundle tmp;

        Bundle bundle = new Bundle();
        Annotation[] annotations = new Annotation[3];
        for(int i=0; i<fields.length; i++){
            tmp = new Bundle();
            //System.out.println("field" +i + " : " + fields[i].get(obj));

            annotations = fields[i].getAnnotations();
            for(int j=0; j<annotations.length; j++){
                //System.out.println(annotations[j].toString());
                //tmp.put(annotations[j].toString());
                fillBundle(tmp,annotations[j]);
            }
            bundle.putBundle(fields[i].getName(),tmp);
        }
        //System.out.println("result maan" + map);
        return bundle;
    }

    public static void fillBundle(Bundle b, Annotation annotation){
        if(annotation instanceof RegexValidator){
            b.putString("RegexValidator", ((RegexValidator) annotation).regex());
        }else if(annotation instanceof RangeValidator){
            b.putString("RangeValidator", Arrays.toString(((RangeValidator) annotation).range()));
        }else if(annotation instanceof Description){
            b.putString("Description", ((Description) annotation).description());
        }else if(annotation instanceof ClassValidator){
            b.putString("ClassValidator", ((ClassValidator) annotation).classRef());
        }else if(annotation instanceof PhoneNumber){
            b.putDouble("PhoneNumber", ((PhoneNumber) annotation).phoneNumber());
        }else if(annotation instanceof GeoCoordinates){
            b.putString("GeoCoordinates", Arrays.toString(((GeoCoordinates) annotation).geo()));
        }
    }


    public static void registerObjects(Bundle b, List objs) throws IllegalAccessException {

        for(Object obj: objs){
            Bundle tmp = readConstraints(obj);
            String classText = obj.getClass().toString();
            int pos = classText.lastIndexOf(".") +1;
            classText = classText.substring(pos);

            b.putBundle(classText,tmp);
        }

    }
}
