package fr.uge.confroid.lib;

import android.os.Bundle;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public final class BundleToObject {

    @SuppressWarnings("unchecked")
    public static <T> T extract(Bundle b, String key) {
        Map<Integer, Object> alreadyVisited = new HashMap<>();
        try {
            return (T) extractRecursive(b, key, alreadyVisited);
        } catch (ReflectiveOperationException e) {
            throw new RuntimeException(e);
        }
    }

    private static Object extractRecursive(Bundle b, String key, Map<Integer, Object> alreadyVisited) throws ReflectiveOperationException {
        Object obj = b.get(key);
        if (obj instanceof Bundle) {
            Bundle sub = (Bundle) obj;
            int refId = sub.getInt("ref", -1);
            if (refId == -1) {
                int id = sub.getInt("id");
                Class<?> clazz = Class.forName(Objects.requireNonNull(sub.getString("class")));
                Object result = clazz.getConstructor().newInstance();
                alreadyVisited.put(id, result);
                Field[] fields = clazz.getDeclaredFields();
                for (Field field : fields) {
                    if (Modifier.isTransient(field.getModifiers())
                            || Modifier.isFinal(field.getModifiers())
                            || Modifier.isStatic(field.getModifiers())) {
                        continue;
                    }
                    field.setAccessible(true);
                    String name = field.getName();
                    Object value = extractRecursive(sub, name, alreadyVisited);
                    field.set(result, value);
                }
                return result;
            } else {
                return alreadyVisited.get(refId);
            }
        } else {
            return obj;
        }
    }
}
