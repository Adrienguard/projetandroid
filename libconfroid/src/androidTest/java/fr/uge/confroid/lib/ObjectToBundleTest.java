package fr.uge.confroid.lib;

import android.os.Bundle;
import android.util.Log;

import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ObjectToBundleTest {

    @Test
    public void test() {
        Bundle b = new Bundle();
        ObjectToBundle.fillBundle(b, "content", new A());
        Log.i("ObjectToBundleTest", b.toString());
        Bundle result = b.getBundle("content");
        assertNotNull(result);
        assertEquals(1, result.getInt("id"));
        assertEquals("foo", result.getString("a"));
    }

    public static class A {
        private String a = "foo";
        private B b = new B();
    }

    public static class B {
        private String b = "bar";
        private int c = 42;
        private double d = 3.14;
    }
}
