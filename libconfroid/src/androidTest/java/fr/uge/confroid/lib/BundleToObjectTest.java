package fr.uge.confroid.lib;

import android.os.Bundle;
import android.util.Log;

import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.jetbrains.annotations.NotNull;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class BundleToObjectTest {

    @Test
    public void test() {
        Bundle b = new Bundle();
        b.putInt("id", 2);
        b.putString("class", "fr.uge.confroid.lib.BundleToObjectTest$B");
        b.putString("b", "reddit");
        b.putInt("c", 1337);
        b.putDouble("d", 999.9);
        Bundle a = new Bundle();
        a.putInt("id", 1);
        a.putString("class", "fr.uge.confroid.lib.BundleToObjectTest$A");
        a.putString("a", "twitter");
        a.putBundle("b", b);
        Bundle container = new Bundle();
        container.putBundle("content", a);
        A obj = BundleToObject.extract(container, "content");
        assertNotNull(obj);
        Log.i("BundleToObjectTest", obj.toString());
        assertEquals("twitter", obj.a);
        assertNotNull(obj.b);
        assertEquals("reddit", obj.b.b);
        assertEquals(1337, obj.b.c);
        assertEquals(999.9, obj.b.d, 0.1);
    }

    public static class A {
        private String a = "foo";
        private B b = new B();


        @NotNull
        @Override
        public String toString() {
            return "A{" +
                    "a='" + a + '\'' +
                    ", b=" + b +
                    '}';
        }
    }

    public static class B {
        private String b = "bar";
        private int c = 42;
        private double d = 3.14;

        @NotNull
        @Override
        public String toString() {
            return "B{" +
                    "b='" + b + '\'' +
                    ", c=" + c +
                    ", d=" + d +
                    '}';
        }
    }
}
