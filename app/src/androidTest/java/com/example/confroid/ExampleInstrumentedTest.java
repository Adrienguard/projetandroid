package com.example.confroid;

import android.content.Context;


import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import okhttp3.HttpUrl;
import okhttp3.mockwebserver.Dispatcher;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;

import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {

    final Dispatcher dispatcher = new Dispatcher() {

        @Override
        public MockResponse dispatch (RecordedRequest request) throws InterruptedException {

            switch (request.getPath()) {
                case "/test/login/auth/":
                    return new MockResponse().setResponseCode(200);
                case "/test/check/version/":
                    return new MockResponse().setResponseCode(200).setBody("version=9");
                case "/test/profile/info":
                    return new MockResponse().setResponseCode(200).setBody("{\\\"info\\\":{\\\"name\":\"Lucas Albuquerque\",\"age\":\"21\",\"gender\":\"male\"}}");
            }
            return new MockResponse().setResponseCode(404);
        }
    };



    @Test
    public void useAppContext() {
        // Context of the app under test.

        //Context appContext = InstrumentationRegistry.getTargetContext();

        //assertEquals("com.example.myfirstapp", appContext.getPackageName());


    }




    @Test
    public void test() throws Exception {
        // Create a MockWebServer. These are lean enough that you can create a new
        // instance for every unit test.
        MockWebServer server = new MockWebServer();


        // Schedule some responses.
        server.enqueue(new MockResponse().setBody("hello, world!"));
        server.enqueue(new MockResponse().setBody("sup, bra?"));
        server.enqueue(new MockResponse().setBody("yo dog"));

        // Start the server.
        server.start(8080);

        // Ask the server for its URL. You'll need this to make HTTP requests.
        HttpUrl baseUrl = server.url("/test/storage/");
        MockResponse response = new MockResponse()
                .addHeader("Content-Type", "application/json; charset=utf-8")
                .addHeader("Cache-Control", "no-cache")
                .setBody("{}");

        server.setDispatcher(dispatcher);

        // Exercise your application code, which should make those HTTP requests.
        // Responses are returned in the same order that they are enqueued.
        /*Chat chat = new Chat(baseUrl);

        chat.loadMore();
        assertEquals("hello, world!", chat.messages());

        chat.loadMore();
        chat.loadMore();
        assertEquals(""
                + "hello, world!\n"
                + "sup, bra?\n"
                + "yo dog", chat.messages());*/
        System.out.println(baseUrl.toString());



        // Optional: confirm that your app made the HTTP requests you were expecting.
        RecordedRequest request1 = server.takeRequest();
        System.out.println("pogu");
        assertEquals("/test/storage/messages/", request1.getPath());
        System.out.println(request1.toString());
        assertNotNull(request1.getHeader("Authorization"));

        RecordedRequest request2 = server.takeRequest();
        assertEquals("/test/storage/messages/2", request2.getPath());

        RecordedRequest request3 = server.takeRequest();
        assertEquals("/v1/chat/messages/3", request3.getPath());

        // Shut down the server. Instances cannot be reused.
        server.shutdown();
    }
}
