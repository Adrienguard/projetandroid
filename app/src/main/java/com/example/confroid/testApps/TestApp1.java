package com.example.confroid.testApps;

import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.confroid.R;
import com.example.confroid.receivers.TokenDispenser;
import com.example.confroid.services.ConfigurationPuller;
import com.example.confroid.services.ConfigurationPusher;
import com.example.confroid.services.ConfigurationVersions;
import com.example.confroid.services.ObjectRegistration;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import fr.uge.confroid.lib.Confroid;
import fr.uge.confroid.lib.ObjectToBundle;


public class TestApp1 extends AppCompatActivity {
    private ResultReceiver mResultReceiver;

    public static final String ACTION_CHECK_STATUS = "get_config";

    private String classString = this.getClass().toString();

    //BUTTONS
    Button tokenButton;
    Button saveButton;
    Button getConfigButton;
    Button getVersionButton;
    //TEXTVIEWS
    TextView tokenTextView;
    TextView saveTextview;
    TextView loadConfigTextView;
    TextView getResultTextview;
    //EDITTEXT FOR TOKEN
    EditText tokenEditText;




    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_testapp1);
        tokenButton = findViewById(R.id.tokenButton);
        saveButton = findViewById(R.id.saveConfigButton);
        getConfigButton = findViewById(R.id.getConfigButton);
        getVersionButton = findViewById(R.id.getVersionsButton);

        tokenTextView = findViewById(R.id.textViewToken);
        saveTextview = findViewById(R.id.textViewSave);
        loadConfigTextView = findViewById(R.id.textViewGetConfig);
        getResultTextview = findViewById(R.id.textViewDisplayResult);

        tokenEditText = findViewById(R.id.editTextToken);








        manageConfig();
    }

    @Override
    public void onResume(){
        super.onResume();
        //registerReceiver(broadcastReceiver,new IntentFilter(this.getClass().toString() + "/GET_TOKEN"));
        //registerReceiver(broadcastReceiver,new IntentFilter(this.getClass().toString() + "/PULL_CONFIG"));
        //registerReceiver(broadcastReceiver,new IntentFilter(this.getClass().toString()+"/GET_ALL_VERSIONS"));
    }

    public void manageConfig(){



        //---------------TEST LIB----------------
        Bundle testeB = new Bundle();

        PrefTest p = new PrefTest();
        Car car1 = new Car("clio4",5,new Human("salem","smith",42));
        Car car2 = new Car("OURS",3,new Human("polo","razork",19));
        Car car3 = new Car("CABRIOLET",99,new Human("retardMan","Idiot",50));
        Car car4 = new Car("Carrousel",20,new Human("cocher","cocher",22,true,25.5,"Japanese",652924342),new float[]{15,300});
        Human michel = new Human("michel","dubois",68);

        p.infos.put("voiture1",car1);
        p.infos.put("voiture2", car2);
        p.infos.put("voiture4", car4);
        p.other.add(car3);
        p.other.add(car2);

        ObjectToBundle.fillBundle(testeB,"preferences",p);
        //System.out.println("le resultaaat" +testeB);


        List objects = new ArrayList();
        objects.add(new Car());
        objects.add(new Human());

        Confroid confroid = new Confroid(this.getClass(),this,"ShoppingConf");


        //TOKEN DISPENSER
        tokenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confroid.subscribeConfiguration(TestApp1.this,consumer,TokenDispenser.class);

            }
        });
        // TOKEN A LA MAIN





        //PUSHER
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confroid.saveConfiguration(TestApp1.this,"shoppingPreferences",p,"BGVERSION",ConfigurationPusher.class, getToken(), consumer);
                confroid.registerObjects(TestApp1.this, ObjectRegistration.class,"BBWRXRbyZX",objects);
            }
        });
        //--------------------------------------PULL------------------


        getConfigButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confroid.loadConfiguration(TestApp1.this, "ShoppingConf", "BGVERSION", consumer, ConfigurationPuller.class, getToken());

            }
        });


//---------------------------------LIST ALLL---------------

        getVersionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confroid.getConfigurationVersions(TestApp1.this,"ShoppingConf", consumer,ConfigurationVersions.class, getToken());

            }
        });

    }

    private String getToken(){
        if(tokenEditText.getText().toString().equals("") ){
            return "BBWRXRbyZX";
        }else {
            return tokenEditText.getText().toString();
        }
    }



    private Consumer<String> consumer = new Consumer<String>() {
        @Override
        public void accept(String b) {
            Log.d("Consumer result", b);
            getResultTextview.setText(b);
        }
    };

}
