package com.example.confroid.testApps;

import fr.uge.confroid.lib.annotation.ClassValidator;
import fr.uge.confroid.lib.annotation.Description;
import fr.uge.confroid.lib.annotation.PhoneNumber;
import fr.uge.confroid.lib.annotation.RangeValidator;
import fr.uge.confroid.lib.annotation.RegexValidator;

public class Human {
    @Description(description = "R.string.name")
    @RegexValidator(regex = "[0-9]+")
    public String name;

    @Description(description = "R.string.lastname")
    @RegexValidator(regex = "[0-9]+")
    public String lastname;
    @Description(description = "R.string.age")
    public int age;
    @Description(description = "R.string.alive")
    @ClassValidator(classRef = "CheckIfAlive.class")
    public boolean alive;
    @Description(description = "R.string.identifier")
    @RangeValidator(range = {0,10000000})
    public double identifier;
    @Description(description = "R.string.nationality")
    @RegexValidator(regex = "[0-9]+")
    public String nationality;
    @PhoneNumber
    public int phoneNumber;

    public Human(String name, String lastname, int age) {
        this.name = name;
        this.lastname = lastname;
        this.age = age;
    }

    public Human(String name, String lastname, int age, boolean alive, double identifier, String nationality, int phoneNumber) {
        this.name = name;
        this.lastname = lastname;
        this.age = age;
        this.alive = alive;
        this.identifier = identifier;
        this.nationality = nationality;
        this.phoneNumber = phoneNumber;
    }

    public Human(){

    }
    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", lastname='" + lastname + '\'' +
                ", age=" + age +
                ", alive=" + alive +
                ", identifier=" + identifier +
                ", nationality='" + nationality + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isAlive() {
        return alive;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    public double getIdentifier() {
        return identifier;
    }

    public void setIdentifier(double identifier) {
        this.identifier = identifier;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public int getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
