package com.example.confroid.testApps;

import fr.uge.confroid.lib.annotation.Description;
import fr.uge.confroid.lib.annotation.GeoCoordinates;
import fr.uge.confroid.lib.annotation.RangeValidator;
import fr.uge.confroid.lib.annotation.RegexValidator;

public class Car {
    @Description(description = "R.string.model")
    @RegexValidator(regex = "[0-9]{16}")
    public String model;

    @Description(description = "R.string.doors")
    @RangeValidator(range = {3,5})
    public   int doors;
    @Description(description = "R.string.human")
    public Human human;
    @GeoCoordinates
    public float[] gpsCoordinates;

    public Car(String model, int doors, Human human, float[] gpsCoordinates) {
        this.model = model;
        this.doors = doors;
        this.human = human;
        this.gpsCoordinates =gpsCoordinates;
    }
    public Car(String model, int doors, Human human) {
        this.model = model;
        this.doors = doors;
        this.human = human;
        this.gpsCoordinates = new float[]{0,0};
    }
    public Car(){

    }
    @Override
    public String toString() {
        return "Car{" +
                "model='" + model + '\'' +
                ", doors=" + doors +
                ", human=" + human +
                '}';
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getDoors() {
        return doors;
    }

    public void setDoors(int doors) {
        this.doors = doors;
    }

    public Human getHuman() {
        return human;
    }

    public void setHuman(Human human) {
        this.human = human;
    }
}
