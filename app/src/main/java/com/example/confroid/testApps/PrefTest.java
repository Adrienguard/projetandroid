package com.example.confroid.testApps;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PrefTest {
    public Map<String, Car> infos;
    public List<Car> other;

    public PrefTest(Map<String, Car> infos) {
        this.infos = infos;
    }

    public PrefTest(){
        infos = new HashMap<>();
        other = new ArrayList<>();
    }

    @Override
    public String toString() {
        return "PrefTest{" +
                "infos=" + infos +
                ", other=" + other +
                '}';
    }
}
