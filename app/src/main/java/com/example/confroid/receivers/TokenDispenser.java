package com.example.confroid.receivers;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.ResultReceiver;

import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;

import com.example.confroid.utilities.RandomString;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class TokenDispenser extends JobIntentService {

    public static final String PREFS_NAME = "ConfroidPrefsFile";
    private String storagePath  ="/data/user/0/fr.uge.confroid/files";
    private static AtomicInteger idGenerator = new AtomicInteger();



    public static void enqueueWork(Context context, Intent work) {
        enqueueWork(context, TokenDispenser.class, idGenerator.incrementAndGet(), work);
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        // On stocke les token dans les preferences propres, de confroid  MyprefsFile
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        String token = RandomString.getAlphaNumericString(10);


        Gson gson = new Gson(); // instance de Gson pour la conversion de la map en Json

        // REcupere le string de la map et le convertit en map
        String listTokenMapString = settings.getString("listokens", "zero");
        if(listTokenMapString.equals("zero")){
            listTokenMapString = "{'list': 'vide'}";
        }
        java.lang.reflect.Type type = new TypeToken<HashMap<String, String>>(){}.getType();
        HashMap<String, String> listToken = gson.fromJson(listTokenMapString, type);

        // ajoute (nom_config , token) a la map

        listToken.put(intent.getStringExtra("prefix"), token);

        //enregistre la map dans les pref de confroid

        settings.edit().putString("listokens", gson.toJson(listToken)).apply();

        // Retourne la valeur a l'app client
        Intent response = new Intent();
        response.putExtra("token" , token);
        sendBroadcast(response.setAction("/GET_TOKEN"));


    }
}


