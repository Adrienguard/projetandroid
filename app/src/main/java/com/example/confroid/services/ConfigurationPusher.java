package com.example.confroid.services;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;

import com.example.confroid.utilities.FileManager;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.HashMap;

import fr.uge.confroid.lib.BundleTypeAdapterFactory;

public class ConfigurationPusher extends JobIntentService {

    public static final String PREFS_NAME = "ConfroidPrefsFile";
    private static final int idGenerator = 24999;

    // storage path
    private String storagePath  ="/data/user/0/fr.uge.confroid/files";

    //Gson

    private final Gson gson = new GsonBuilder()
            .registerTypeAdapterFactory(new BundleTypeAdapterFactory())
            .create();





    public static void enqueueWork(Context context, Intent work) {
        Gson gson = new GsonBuilder()
                .registerTypeAdapterFactory(new BundleTypeAdapterFactory())
                .create();
        String json = gson.toJson(work.getBundleExtra("config"));
        work.putExtra("config", json);
        enqueueWork(context, ConfigurationPusher.class, idGenerator, work);
    }


    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        SharedPreferences confSettings = getSharedPreferences(PREFS_NAME, 0);
        File file = new File(storagePath,"Configuration_data");
        //----------------init file store if doesnt exist--------------//
        try {
            init_File(file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        //---------------------------------READ CONFIGURATION FILE---------------//


        String result = FileManager.readFile(file);
        Bundle configurationBundle = gson.fromJson(result, Bundle.class);
        if(configurationBundle == null){configurationBundle = new Bundle();} // init bundle if doesnt exist
        Bundle ba = gson.fromJson(intent.getStringExtra("config"),Bundle.class);

        Bundle content = (Bundle)ba.get("content");
        Bundle b2  = content; //gson.fromJson(content,Bundle.class);

        String json = gson.toJson(ba);

        //-----Authentification-------------------------------------------------------
        String listTokenMapString = confSettings.getString("listokens", "{'list': 'vide'}");
        java.lang.reflect.Type type = new TypeToken<HashMap<String, String>>(){}.getType();
        HashMap<String, String> listToken = gson.fromJson(listTokenMapString, type);


        Intent response = new Intent(); // REPOSNSE FOR RESULT

        if(intent.getStringExtra("token").equals(listToken.get(ba.getString("prefix"))) ||
                intent.getStringExtra("token").equals("M4G1CToK3N")) {


            if(configurationBundle.getBundle(ba.getString("name")) == null){
                Bundle initBundle = new Bundle();
                configurationBundle.putBundle(ba.getString("name"),initBundle);
            }


            //----------Test pour garder un suivi de la dernière version------

            Bundle requestedConfiguration = configurationBundle.getBundle(ba.getString("name"));

            if(requestedConfiguration.getInt("lastVer",-1) == -1){

                requestedConfiguration.putInt("lastVer",0);
            }

            int lastversion = requestedConfiguration.getInt("lastVer",0);//prefbundle.getInt("lastVer",0);


            // DETECTS TAG null or ""
            boolean test = false;
            if(intent.getStringExtra("tag") == null){
                test = false;
            }
            else {
                if(!(intent.getStringExtra("tag").equals(""))){test=true;}
            }

            //TAG TEST
            if ( test ) {

                //detects if tag is redirected or not
                String[] tag;
                if (intent.getStringExtra("tag").contains(":")) {
                    tag = intent.getStringExtra("tag").split(":");
                } else {
                    tag = new String[]{intent.getStringExtra("tag"), "null"};
                }

                if (tag[1].equals("null")) {

                    lastversion += 1;
                    requestedConfiguration.putInt("lastVer", lastversion);

                    b2.putInt("version", lastversion);

                    // date TIMESTAMP UNIX
                    SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
                    sdf.setTimeZone(java.util.TimeZone.getTimeZone("GMT-4"));
                    String formattedDate = sdf.format(System.currentTimeMillis());
                    b2.putString("creationDate", formattedDate);

                    requestedConfiguration.putInt("lastVer", lastversion);
                    requestedConfiguration.putBundle(intent.getStringExtra("tag"), b2);
                    configurationBundle.putBundle(ba.getString("name"),requestedConfiguration);

                    String configurationJson = gson.toJson(configurationBundle);
                    Log.i("RESULT","configuratuin result:  " + configurationJson);
                    FileManager.writeFile(file,configurationJson);

                } else { // PARTIE POUR REDIRIGER VERS LES TAG MAIS PAS SUR QUE CE SOIT DEMANDée EN FIN DE COMPTE

                    /*System.out.println("iiiiiiiiiiiiiiiiiiiiiii" + prefbundle);
                    Object tmp = (String) prefbundle.get(tag[1]);


                    Bundle tmpBundle = gson.fromJson(tmp.toString(), Bundle.class);

                    System.out.println("Bundleeeeeeeeeeeee" + tmpBundle);

                    prefbundle.remove(tag[1]);

                    prefbundle.putString(tag[0], tmp.toString());

                    System.out.println("eeeeeeeeeee" + prefbundle);

                    json = gson.toJson(prefbundle);
                    confSettings.edit().putString(ba.getString("name"), json).apply();*/

                }
            }

            else {

                lastversion +=1;
                b2.putDouble("version",lastversion);

                SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
                sdf.setTimeZone(java.util.TimeZone.getTimeZone("GMT-4"));
                String formattedDate = sdf.format(System.currentTimeMillis());
                b2.putString("creationDate",formattedDate);

                requestedConfiguration.putInt("lastVer", lastversion);
                requestedConfiguration.putBundle(String.valueOf(lastversion), b2);
                configurationBundle.putBundle(ba.getString("name"),requestedConfiguration);

                String configurationJson = gson.toJson(configurationBundle);
                Log.i("RESULT","configuration saved:  " + configurationJson);


                FileManager.writeFile(file,configurationJson);


            }

            sendBroadcast(response.setAction("/SUCCESS"));


        }
        else {
            Log.i("ERROR","WRONG TOKEN");
            sendBroadcast(response.setAction("/WRONG_TOKEN"));
        }


}


    public void init_File(File file)throws IOException{

            if (file.exists() == false) {
                FileWriter fileWriter = null;

                fileWriter = new FileWriter(file);

                BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
                bufferedWriter.write("{}");

                bufferedWriter.close();
            }
    }
}

