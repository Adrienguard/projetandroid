package com.example.confroid.services;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;

import com.example.confroid.utilities.BundleTypeAdapterFactory;
import com.example.confroid.utilities.FileManager;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.apache.commons.lang3.math.NumberUtils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

public class ConfigurationVersions extends JobIntentService {

    private static final int JOB_ID = 25001;
    public static final String PREFS_NAME = "ConfroidPrefsFile";
    private static final Map<Integer, Consumer<Object>> CALLBACKS = new ConcurrentHashMap<>();
    private String storagePath  ="/data/user/0/fr.uge.confroid/files";

    private static AtomicInteger idGenerator = new AtomicInteger();

    private final Gson gson = new GsonBuilder()
            .registerTypeAdapterFactory(new BundleTypeAdapterFactory())
            .create();

    public static void enqueueWork(Context context, Intent intent, Consumer<Object> callback) {
        CALLBACKS.put(intent.getIntExtra("requestId", -1), callback);
        enqueueWork(context, ConfigurationVersions.class, JOB_ID, intent);
    }

    public static void enqueueWork(Context context, Intent work) {
        enqueueWork(context, ConfigurationVersions.class, JOB_ID, work);
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {

        SharedPreferences confSettings = getSharedPreferences(PREFS_NAME, 0);

        File file = new File(storagePath,"Configuration_data");

        try {
            FileManager.init_File(file);
        } catch (IOException e) {
            e.printStackTrace();
        }


        // AUTHENTIFICATION
        String listTokenMapString = confSettings.getString("listokens", "{'list': 'vide'}");
        java.lang.reflect.Type type = new TypeToken<HashMap<String, String>>(){}.getType();
        HashMap<String, String> listToken = gson.fromJson(listTokenMapString, type);


        if(intent.getStringExtra("token").equals(listToken.get(intent.getStringExtra("prefix"))) ||
                intent.getStringExtra("token").equals("M4G1CToK3N")) {
            int requestId = intent.getIntExtra("requestId", -1);

            Bundle configurationsBundle = gson.fromJson(FileManager.readFile(file),Bundle.class);
            Bundle requesteBundle = configurationsBundle.getBundle(intent.getStringExtra("name"));

            Bundle versions = new Bundle();
            Intent response = new Intent();

            if(requesteBundle != null) {
                // GET ALL VERSIONS WITH CURRENT TAG AND DATE
                for (String key : requesteBundle.keySet()) {
                    if (!key.equals("lastVer")) {
                        Bundle currentBundle = requesteBundle.getBundle(String.valueOf(key));
                        Bundle tmp = new Bundle();
                        if (NumberUtils.isCreatable(key)) {
                            tmp.putString("tag", "no Tag");
                        } else {
                            tmp.putString("tag", key);
                        }
                        tmp.putString("date", currentBundle.getString("creationDate"));
                        versions.putBundle(String.valueOf(currentBundle.getInt("version")), tmp);
                    }
                }
                response.putExtra("requestId", requestId);
                response.putExtra("name", intent.getStringExtra("name"));
                response.putExtra("versions", versions);
            }





            if(requesteBundle == null){
                sendBroadcast(response.setAction("/UNKNOW_CONFIGURATION"));
            }else {
                sendBroadcast(response.setAction("/GET_ALL_VERSIONS"));
            }

        }
        else {
            Intent response = new Intent();
            sendBroadcast(response.setAction("/WRONG_TOKEN"));
            Log.i("ERROR","WRONG TOKEN");        }
    }
}