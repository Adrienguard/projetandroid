package com.example.confroid.services;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.ResultReceiver;

import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;

import com.example.confroid.utilities.FileManager;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import fr.uge.confroid.lib.BundleTypeAdapterFactory;

public class ObjectRegistration extends JobIntentService {

    public static final String PREFS_NAME = "ConfroidPrefsFile";
    private static final int idGenerator = 25002;

    private String storagePath  ="/data/user/0/fr.uge.confroid/files";

    private final Gson gson = new GsonBuilder()
            .registerTypeAdapterFactory(new BundleTypeAdapterFactory())
            .create();



    public static void enqueueWork(Context context, Intent work) {
        Intent intent = new Intent(context, ConfigurationPusher.class);
        intent.putExtra("receiver", work);
        enqueueWork(context, ObjectRegistration.class, idGenerator, intent);
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        SharedPreferences confSettings = getSharedPreferences(PREFS_NAME, 0);

        String listTokenMapString = confSettings.getString("listokens", "{'list': 'vide'}");
        java.lang.reflect.Type type = new TypeToken<HashMap<String, String>>(){}.getType();
        HashMap<String, String> listToken = gson.fromJson(listTokenMapString, type);


        if(intent.getStringExtra("token").equals(listToken.get(intent.getStringExtra("prefix"))) ||
                intent.getStringExtra("token").equals("M4G1CToK3N")) {
            File fileObjects = new File(storagePath, "Registered_objects");


            // STORE REGISTERED OBJECTS

            try {
                FileManager.init_File(fileObjects);
            } catch (IOException e) {
                e.printStackTrace();
            }

            String result = FileManager.readFile(fileObjects);
            Bundle objectsStoredBundle = gson.fromJson(result, Bundle.class);
            Bundle registeredObjects = gson.fromJson(intent.getStringExtra("registeredObjects"), Bundle.class);
            for(String key : registeredObjects.keySet()){
                objectsStoredBundle.putBundle(key,registeredObjects.getBundle(key));
            }


            FileManager.writeFile(fileObjects, gson.toJson(objectsStoredBundle));

        }
        else {
            Intent response = new Intent();
            sendBroadcast(response.setAction("/WRONG_TOKEN"));
        }

    }
}
