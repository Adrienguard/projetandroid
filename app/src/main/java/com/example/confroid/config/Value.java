package com.example.confroid.config;

import android.os.Bundle;

public interface Value {
    void addToBundle(Bundle bundle, String key);
}
