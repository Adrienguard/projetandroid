package com.example.confroid.config;

public class Configuration {
    private Value value;

    public Configuration(Value value) {
        this.value = value;
    }

    public Value getValue() {
        return value;
    }

    public void setValue(Value value) {
        this.value = value;
    }
}
