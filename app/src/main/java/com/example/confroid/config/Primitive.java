package com.example.confroid.config;

import android.os.Bundle;

public class Primitive implements Value {
    private Object primitive;

    public Primitive(Object primitive) {
        this.primitive = primitive;
    }

    @Override
    public void addToBundle(Bundle bundle, String key) {
        if(primitive instanceof String){
            bundle.putString(key,(String) primitive);
        }
        if(primitive instanceof byte[]){
            bundle.putByteArray(key,(byte[]) primitive);
        }
        if(primitive instanceof Integer){
            bundle.putInt(key,(Integer) primitive);
        }
        if(primitive instanceof Double){
            bundle.putDouble(key,(Double) primitive);
        }
        if(primitive instanceof Long){
            bundle.putLong(key,(Long) primitive);
        }
        if(primitive instanceof Float){
            bundle.putFloat(key,(Float) primitive);
        }
        if(primitive instanceof Boolean){
            bundle.putBoolean(key,(Boolean) primitive);
        }

    }
}
