package com.example.confroid.config;

import android.os.Bundle;

import java.util.Map;

public class Dictionary implements Value {
    private Map<String,Value> valueMap;

    public Dictionary(Map<String, Value> valueMap) {
        this.valueMap = valueMap;
    }

    @Override
    public void addToBundle(Bundle bundle, String key) {
        Bundle b = new Bundle();
        for (Map.Entry<String,Value> entry: valueMap.entrySet()){
            entry.getValue().addToBundle(b,entry.getKey());
        }
        bundle.putBundle(key,b);
    }
}
