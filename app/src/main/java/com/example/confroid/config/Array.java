package com.example.confroid.config;

import android.os.Bundle;

import java.util.List;

public class Array implements Value {

    private List<Value> valueList;

    public Array(List<Value> valueList) {
        this.valueList = valueList;
    }

    @Override
    public void addToBundle(Bundle bundle, String key) {
        Bundle b = new Bundle();
        for(int i =0; i< valueList.size(); i++){
                valueList.get(i).addToBundle(b, ""+i);
        }
        bundle.putBundle(key,b);
    }
}
