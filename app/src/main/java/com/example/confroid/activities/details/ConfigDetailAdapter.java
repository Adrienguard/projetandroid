package com.example.confroid.activities.details;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.confroid.R;
import com.example.confroid.activities.ClickListener;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

public class ConfigDetailAdapter extends RecyclerView.Adapter<ConfigDetailAdapter.ViewHolder> {

    private List<String> versions;
    private static ClickListener clickListener;
    private Context context;


    public ConfigDetailAdapter(List<String> versions, ClickListener listener, Context context) {
        this.versions = versions;
        clickListener = listener;
        this.context = context;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.version_visual, viewGroup, false));
    }



    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.update(versions.get(i));
    }

    @Override
    public int getItemCount() {
        return versions.size();
    }


    public  class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView name;
        private TextView versionName;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            versionName = itemView.findViewById(R.id.version_name);
            name = itemView.findViewById(R.id.config_name);

            itemView.setOnClickListener(this);
        }


        public void update(String config) {

            Gson gson = new GsonBuilder().setPrettyPrinting().create();

            String[] content = config.split("§§");


            name.setText(content[1]);
            versionName.setText(content[0]);

        }

        // CLICK DUNE RECHERCHE ET LANCEMENT DU SITE WEB
        @Override
        public void onClick(View v) {
            clickListener.onResultClick(v, this.getAdapterPosition());

            String res =versions.get(getAdapterPosition());
            ConfiguartionDetailsActivity.modif.setText(res);
            ConfiguartionDetailsActivity.modif.setVisibility(View.VISIBLE);
            ConfiguartionDetailsActivity.buttons.setVisibility(View.VISIBLE);

        }

    }


    public List<String> getResults() {
        return versions;
    }
    public void setResults(List<String> configs) {
        this.versions = configs;
    }


}
