package com.example.confroid.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;


import com.example.confroid.R;
import com.example.confroid.activities.restoring.RestoreConfigurationsActivity;
import com.example.confroid.activities.web.WebExportImport;
import com.example.confroid.utilities.FileManager;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import fr.uge.confroid.lib.BundleTypeAdapterFactory;

public class MainActivity extends AppCompatActivity implements ClickListener  {

    private TextView textView;
    private List<String> results = new ArrayList<>();
    private ConfigAdapter configAdapter;
    private RecyclerView recyclerView;
    private SharedPreferences settings;
    public static final String PREFS_NAME = "ConfroidPrefsFile";
    public static final String CONFIGS = "configurations";
    private String storagePath  ="/data/user/0/fr.uge.confroid/files";
    private static final int FILE_EXPORT_REQUEST_CODE = 12;
    private static final int FILE_IMPORT_REQUEST_CODE = 13;

    private SharedPreferences config;// = getSharedPreferences(CONFIGS, 0);;
    private final Gson gson = new GsonBuilder()
            .registerTypeAdapterFactory(new BundleTypeAdapterFactory())
            .create();

    //receiver recuperant le resultat du service


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // INTRO SCREEEN



        setContentView(R.layout.activity_main);


        File file = new File(this.getFilesDir().getPath(),"Configuration_data");
        File fileObjects = new File(storagePath, "Registered_objects");
        try {
            FileManager.init_File(file);
            FileManager.init_File(fileObjects);
        } catch (IOException e) {
            e.printStackTrace();
        }

        settings = getSharedPreferences(PREFS_NAME, 0);


        recyclerView = findViewById(R.id.recyclerView);
        configAdapter= new ConfigAdapter( results, this, this.getApplicationContext(),settings,file);

        recyclerView.setAdapter(configAdapter);
        updateLayoutManager(null);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                LinearLayoutManager.VERTICAL);
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.divider, null));
        recyclerView.addItemDecoration(dividerItemDecoration);


        // ADAPTER LIST FILLED WITH CONFIGS
        List l = new ArrayList();

        Bundle cofigurationsBundle = gson.fromJson(FileManager.readFile(file),Bundle.class);
        for(String key: cofigurationsBundle.keySet()){
            if(!key.equals("listokens")) {
                l.add(key);
            }
        }

        configAdapter.setResults(l);

    }

    @Override
    public void onResume(){
        List l = new ArrayList();
        super.onResume();
        File file = new File(this.getFilesDir().getPath(),"Configuration_data");

        Bundle cofigurationsBundle = gson.fromJson(FileManager.readFile(file),Bundle.class);
        for(String key: cofigurationsBundle.keySet()){
            if(!key.equals("listokens")) {
                l.add(key);
            }
        }
        configAdapter.setResults(l);
        configAdapter.notifyDataSetChanged();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FILE_EXPORT_REQUEST_CODE) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    if (data != null
                            && data.getData() != null) {
                        try {
                            writeInFile(data.getData(), fileContentRead());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case Activity.RESULT_CANCELED:
                    break;
            }
        }
        if (requestCode == FILE_IMPORT_REQUEST_CODE) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    if (data != null
                            && data.getData() != null) {
                        try {
                            Intent i = new Intent(this, RestoreConfigurationsActivity.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            i.putExtra("Configs",readTextFromUri(data.getData()));
                            //i.putExtra("configKey", configs.get(getAdapterPosition()));
                            startActivity(i);

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case Activity.RESULT_CANCELED:
                    break;
            }
        }
    }


    // DEMARRAGE DE l'EXPORT SI CHAMP NON VIDE
    public void exportConfigs(View v){


        EditText configname = (EditText) findViewById(R.id.editTextRequest);
        if(!configname.getText().equals("")) {
            Intent exportIntent = new Intent(Intent.ACTION_CREATE_DOCUMENT);
            exportIntent.addCategory(Intent.CATEGORY_OPENABLE);
            exportIntent.setType("text/plain");
            String filename =  configname.getText().toString();
            exportIntent.putExtra(Intent.EXTRA_TITLE, filename);
            startActivityForResult(exportIntent, FILE_EXPORT_REQUEST_CODE);
        }

    }

    public void importConfigs(View v){

        EditText configname = (EditText) findViewById(R.id.editTextRequest);
        if(!configname.getText().equals("")) {
            Intent exportIntent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            exportIntent.addCategory(Intent.CATEGORY_OPENABLE);
            exportIntent.setType("text/plain");
            String filename =  configname.getText().toString();
            exportIntent.putExtra(Intent.EXTRA_TITLE, filename);
            startActivityForResult(exportIntent, FILE_IMPORT_REQUEST_CODE);
        }

    }

    public void webSetup(View v){

        Intent intent = new Intent(this, WebExportImport.class);
        startActivity(intent);

    }





    private Activity getActivity(){
        return this;
    }

    private void writeInFile(@NonNull Uri uri, @NonNull String text) throws IOException{
        OutputStream outputStream;

        outputStream = getContentResolver().openOutputStream(uri);
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream));
        writer.write(text);
        writer.flush();
        writer.close();

    }

    private String readTextFromUri(Uri uri) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        try (InputStream inputStream =
                     getContentResolver().openInputStream(uri);
             BufferedReader reader = new BufferedReader(
                     new InputStreamReader(Objects.requireNonNull(inputStream)))) {
            String line;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
            }
        }
        return stringBuilder.toString();
    }

    public String fileContentRead(){

        File file = new File(this.getFilesDir().getPath(),"Configuration_data");

        return FileManager.readFile(file);
    }



    private RecyclerView.LayoutManager createLayoutManager() {
        RecyclerView.LayoutManager lm = null;

        return  new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
    }


    private void updateLayoutManager(View v) {
        recyclerView.setLayoutManager(createLayoutManager());
    }


    @Override
    public void onResultClick(View view, int position) {

    }


}