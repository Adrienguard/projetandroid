package com.example.confroid.activities;

import android.view.View;

public interface ClickListener  {
    void onResultClick(View view, int position);
}
