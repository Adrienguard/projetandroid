package com.example.confroid.activities.customViews;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.example.confroid.R;
import com.example.confroid.activities.ClickListener;
import com.example.confroid.activities.details.ConfigDetailAdapter;
import com.example.confroid.activities.details.ConfiguartionDetailsActivity;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

public class FragmentAdapter extends RecyclerView.Adapter<FragmentAdapter.ViewHolder> {

    private List<String> fields;
    private static ClickListener clickListener;
    private Context context;

    private Bundle subBundle;
    private String tag;


    public FragmentAdapter(List<String> fields, ClickListener listener, Context context, Bundle subBundle, String tag) {
        this.fields = fields;
        clickListener = listener;
        this.context = context;
        this.subBundle = subBundle;
        this.tag = tag;

    }

    @NonNull
    @Override
    public FragmentAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new FragmentAdapter.ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_fragment_visual, viewGroup, false));
    }




    @Override
    public void onBindViewHolder(@NonNull FragmentAdapter.ViewHolder viewHolder, int i) {

        viewHolder.update(fields.get(i));
    }

    @Override
    public int getItemCount() {
        return fields.size();
    }


    public  class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView name;
        private TextView classTextview;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.field_name);
            classTextview = itemView.findViewById(R.id.classTextview);

            itemView.setOnClickListener(this);


            // LONG CLICK TO DELETE VALUE
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(context);

                    builder.setTitle(fields.get(getAdapterPosition()));
                    builder.setPositiveButton("delete", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    subBundle.remove(fields.get(getAdapterPosition()));
                                    fields.remove(getAdapterPosition());
                                    setResults(fields);
                                    notifyDataSetChanged();
                                    dialog.cancel();
                                }
                            });

                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    dialog.cancel();
                                }
                            });
                    builder.show();
                    return true;
                }
            });
        }

        // DIFFERENCIE UNE MAP D'UNE LISTE D'UNE PRIMITIVE, D'un Objet personalisé puis met a jour les champs
        public void update(String field) {
            name.setText(field);
            if(subBundle.get(field) instanceof Bundle){
                if(subBundle.getBundle(field).containsKey("class")){
                    String classText = subBundle.getBundle(field).getString("class");
                    int pos = classText.lastIndexOf(".") +1;
                    classText = classText.substring(pos);
                    name.setText(field);
                    classTextview.setText(classText);
                }
                else if(subBundle.getBundle(field).containsKey("0")){// ça veut dire que c'est une liste
                    classTextview.setText("LIST");
                    classTextview.setBackgroundColor(Color.YELLOW);
                }
                else {
                    classTextview.setVisibility(View.GONE);
                }
            }
            else {
                classTextview.setVisibility(View.GONE);
            }
        }

        // Lance un nouveau fragment si c'est pas une primitive, un fragment d'edition sinon
        @Override
        public void onClick(View v) {
            clickListener.onResultClick(v, this.getAdapterPosition());

            System.out.println(subBundle.get(fields.get(getAdapterPosition())));
            AppCompatActivity activity = (AppCompatActivity) v.getContext();

            tag = String.valueOf(Integer.valueOf(tag) + 1);

            if(subBundle.get(fields.get(getAdapterPosition())) instanceof Bundle){

                activity.getSupportFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).
                        add(R.id.customViewConstraintLayout, ConfigurationFragment.newInstance(subBundle.getBundle(fields.get(getAdapterPosition())),tag,   fields.get(getAdapterPosition())))
                        .addToBackStack(tag)
                        .commit();
            }
            else{
                String classString ="no description";
                if(subBundle.containsKey("class")){
                    classString = subBundle.getString("class");
                    int pos = classString.lastIndexOf(".") +1;
                    classString = classString.substring(pos);

                };
                activity.getSupportFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).
                        add(R.id.customViewConstraintLayout, FieldEditFragment.newInstance(fields.get(getAdapterPosition()),subBundle.get(fields.get(getAdapterPosition())), tag, classString))
                        .addToBackStack(tag)
                        .commit();
            }
        }

    }

    public List<String> getResults() {
        return fields;
    }
    public void setResults(List<String> fields) {
        this.fields = fields;
    }


}

