package com.example.confroid.activities.details;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.example.confroid.R;
import com.example.confroid.activities.ClickListener;
import com.example.confroid.services.ConfigurationPusher;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import fr.uge.confroid.lib.BundleTypeAdapterFactory;

public class ConfiguartionDetailsActivity extends AppCompatActivity implements ClickListener {

    private final Gson gson = new GsonBuilder()
            .registerTypeAdapterFactory(new BundleTypeAdapterFactory())
            .create();

    private List<String> versions = new ArrayList<>();
    private ConfigDetailAdapter configAdapter;
    private RecyclerView recyclerView;

    public static final String PREFS_NAME = "ConfroidPrefsFile";
    SharedPreferences settings;

    public static EditText modif;
    public static LinearLayout buttons;

    String key;


    @Override
    protected void onCreate(Bundle savedInstanceState)  {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuartion_details);
        modif = findViewById(R.id.textArea_information);
        buttons = findViewById(R.id.linearLayoutVersionButtons);
        modif.setVisibility(View.GONE);
        buttons.setVisibility(View.GONE);


        //PERMET DE MAINTENIR L'edition a la rotation de l'écran
        if (savedInstanceState != null ) {
            if(savedInstanceState.getBoolean("visible") == true) {
                modif.setVisibility(View.VISIBLE);
                buttons.setVisibility(View.VISIBLE);
                modif.setText(savedInstanceState.getString("currentEdition"));
            }
        }

        settings = getSharedPreferences(PREFS_NAME, 0);

        recyclerView = findViewById(R.id.recyclerView);


        String content = getIntent().getStringExtra("content");
        key = getIntent().getStringExtra("configKey");

        // SETUP L'AFFICHAGE DES CONFIGURATIONS
        try {
            traitement(content);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        configAdapter= new ConfigDetailAdapter( versions, this, this.getApplicationContext());

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                LinearLayoutManager.VERTICAL);
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.divider, null));
        recyclerView.addItemDecoration(dividerItemDecoration);

        recyclerView.setAdapter(configAdapter);
        updateLayoutManager(null);
    }


    public void traitement(String content) throws JSONException{

        JSONObject jsonObject = null;

        jsonObject = new JSONObject(content);

        Iterator<String> keys = jsonObject.keys();

        while(keys.hasNext()) {
            String key = keys.next();
            if (jsonObject.get(key) instanceof JSONObject) {
                JSONObject affichage = (JSONObject) jsonObject.get(key);
                versions.add(key + "§§" + affichage.toString(4));
            }
        }
    }

    public void back(View v) {
        modif.setVisibility(View.GONE);
        buttons.setVisibility(View.GONE);
    }

    public void validate(View v) throws JSONException {
        modif.setVisibility(View.GONE);
        buttons.setVisibility(View.GONE);


        String res = modif.getText().toString();
        res = res.split("§§")[1];

        buttons.setVisibility(View.GONE);

        Bundle b = gson.fromJson(res, Bundle.class);
        Bundle config = new Bundle();
        config.putBundle("content", b);

        String configName = "Reviewer";
        config.putString("name", key);



        Intent i = new Intent();
        i.putExtra("config", config);
        i.putExtra("token","M4G1CToK3N");
        i.putExtra("tag","");


        ConfigurationPusher.enqueueWork(ConfiguartionDetailsActivity.this, i);

        configAdapter.notifyDataSetChanged();
    }

    private RecyclerView.LayoutManager createLayoutManager() {
        RecyclerView.LayoutManager lm = null;

        return  new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
    }


    private void updateLayoutManager(View v) {
        recyclerView.setLayoutManager(createLayoutManager());
    }

    @Override
    public void onResultClick(View view, int position) {

    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        EditText contenu = findViewById(R.id.textArea_information);
        savedInstanceState.putString("currentEdition",contenu.getText().toString());
        if(buttons.getVisibility()== View.VISIBLE){
            savedInstanceState.putBoolean("visible", true);
        }
        else { savedInstanceState.putBoolean("visible", false);}
    }
}