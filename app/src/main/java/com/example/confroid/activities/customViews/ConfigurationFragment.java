package com.example.confroid.activities.customViews;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentResultListener;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.confroid.R;
import com.example.confroid.activities.ClickListener;
import com.example.confroid.activities.ConfigAdapter;
import com.example.confroid.utilities.BundleTypeAdapterFactory;

import java.util.ArrayList;
import java.util.List;

public class ConfigurationFragment extends Fragment implements ClickListener {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";

    // TODO: Rename and change types of parameters
    private Bundle mParam1; // Bundle containing config
    private String mParam2; // key of the bundle
    private String mParam3; // id of fragment

    private FragmentAdapter fragmentAdapter;
    private RecyclerView recyclerView;
    private List displayableStuff = new ArrayList();
    TextView parentField;
    String parentFieldString;

    public ConfigurationFragment() {
        // Required empty public constructor
    }


    public static ConfigurationFragment newInstance(Bundle param1, String param2, String param3) {
        ConfigurationFragment fragment = new ConfigurationFragment();
        Bundle args = new Bundle();
        args.putBundle(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        args.putString(ARG_PARAM3, param3);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FragmentManager fmManager = getParentFragmentManager();


        if (getArguments() != null) {
            mParam1 = getArguments().getBundle(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            mParam3 = getArguments().getString(ARG_PARAM3);
        }
        System.out.println(mParam1);
        System.out.println(mParam2);
        System.out.println(mParam3);


        printRealParam();
        String rkey = (String.valueOf(Integer.valueOf(mParam2)+1)); // key from child fragment

        // FIELD EDIT ANSWER Update
        getParentFragmentManager().setFragmentResultListener("requestKey"+rkey,this, new FragmentResultListener() {
            @Override
            public void onFragmentResult(@NonNull String requestKey, @NonNull Bundle bundle) {
                mParam1.remove(bundle.getString("ancientField"));
                mParam1.putBundle(bundle.getString("replacedField"),bundle.getBundle("content"));
                List<String> addElementList = new ArrayList<>();
                for(String key: mParam1.keySet()){
                    addElementList.add(key);
                }
                fragmentAdapter.setResults(addElementList);
                fragmentAdapter.notifyDataSetChanged();
            }
        });

        // VALUE EDIT ANSWER UPdate
        getParentFragmentManager().setFragmentResultListener("requestKey2"+rkey,this, new FragmentResultListener() {
            @Override
            public void onFragmentResult(@NonNull String requestKey, @NonNull Bundle bundle) {
                mParam1.remove(bundle.getString("ancientKey"));
                BundleTypeAdapterFactory.fillBundleByTypeAndStringObject(mParam1,bundle.getString("content"),bundle.getString("type"),bundle.getString("key"));
                //mParam1.put(bundle.getString("ancientField"));
                List<String> addElementList = new ArrayList<>();
                for(String key: mParam1.keySet()){
                    addElementList.add(key);
                }
                fragmentAdapter.setResults(addElementList);
                fragmentAdapter.notifyDataSetChanged();
            }
        });


    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_configuration, container, false);
        recyclerView = rootView.findViewById(R.id.recyclerFragment);

        parentField = rootView.findViewById(R.id.parentField);

        parentField.setText(mParam3);
        parentFieldString = mParam3;

        for(String key: mParam1.keySet()){
            displayableStuff.add(key);
        }

        fragmentAdapter= new FragmentAdapter( displayableStuff, this, getContext(), mParam1,mParam2);

        recyclerView.setAdapter(fragmentAdapter);
        updateLayoutManager(null);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                LinearLayoutManager.VERTICAL);
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.divider, null));
        recyclerView.addItemDecoration(dividerItemDecoration);


        // ADD NEW ENTRY TO BUNDLE showing current scope fields
        Button button = (Button) rootView.findViewById(R.id.button5);
        button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                List<String> addElementList = new ArrayList<>();
                mParam1.putString("NEW ENTRY", "null");


                for(String key: mParam1.keySet()){
                    addElementList.add(key);
                }
                fragmentAdapter.setResults(addElementList);
                fragmentAdapter.notifyDataSetChanged();

            }

        });

        // CONFIRM CHANGES TO SAVE THEM IN INTERNAL STORAGE, CLOSES ALL FRAGMENTS
        Button confirmButton = (Button) rootView.findViewById(R.id.confirmButton);
        confirmButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Bundle newBundle = new Bundle();

                newBundle.putBundle("content",mParam1);
                newBundle.putString("replacedField", parentField.getText().toString());
                newBundle.putString("ancientField", parentFieldString);
                String rkey = (String.valueOf(Integer.valueOf(mParam2)));
                getParentFragmentManager().setFragmentResult("requestKey"+rkey , newBundle);


                FragmentManager fmManager = getParentFragmentManager();
                // SAVE FRAGMENT
                ((CustomView)getActivity()).saveBundle();
                if (fmManager.getBackStackEntryCount() > 0) {
                    fmManager.popBackStack(fmManager.getBackStackEntryAt(0).getId(), fmManager.POP_BACK_STACK_INCLUSIVE);

                }


            }

        });
        return rootView;
    }

    private RecyclerView.LayoutManager createLayoutManager() {
        RecyclerView.LayoutManager lm = null;

        return  new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
    }


    private void updateLayoutManager(View v) {
        recyclerView.setLayoutManager(createLayoutManager());
    }

    @Override
    public void onResultClick(View view, int position) {

    }
    private void printRealParam(){
        Log.i("Real PARAM", mParam1.toString());

    }






}