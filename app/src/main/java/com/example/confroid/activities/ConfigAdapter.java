package com.example.confroid.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.example.confroid.R;
import com.example.confroid.activities.customViews.CustomView;
import com.example.confroid.activities.details.ConfiguartionDetailsActivity;
import com.example.confroid.utilities.FileManager;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.util.List;

import fr.uge.confroid.lib.BundleTypeAdapterFactory;

public class ConfigAdapter extends RecyclerView.Adapter<ConfigAdapter.ViewHolder> {

    private List<String> configs;
    private static ClickListener clickListener;
    private Context context;
    private File file;


    SharedPreferences settings;

    private final Gson gson = new GsonBuilder()
            .registerTypeAdapterFactory(new BundleTypeAdapterFactory())
            .create();


    public ConfigAdapter(List<String> configs, ClickListener listener, Context context, SharedPreferences preferences, File file) {
        this.configs = configs;
        clickListener = listener;
        this.context = context;
        this.settings = preferences;
        this.file = file;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.config_visual, viewGroup, false));
    }



    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.update(configs.get(i));
    }

    @Override
    public int getItemCount() {
        return configs.size();
    }


    public  class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView name;
        private TextView url;
        private TextView desc;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.config_name);

            itemView.setOnClickListener(this);
        }


        public void update(String config) {

            name.setText(config);

        }

        // CLICK DUNE RECHERCHE ET LANCEMENT DU SITE WEB
        @Override
        public void onClick(View v) {
            clickListener.onResultClick(v, this.getAdapterPosition());
            Bundle configurationsBundle = gson.fromJson(FileManager.readFile(file), Bundle.class);

            String content = gson.toJson(configurationsBundle.getBundle(configs.get(getAdapterPosition())));

            // SELECT EDITION MODE
            AlertDialog.Builder builder
                    = new AlertDialog
                    .Builder(v.getContext());

            // Set the message show for the edition type time
            builder.setMessage("select edition mode");
            builder.setTitle("Edition mode");
            builder.setPositiveButton(
                            "classic",
                            new DialogInterface
                                    .OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    Intent i = new Intent(context, ConfiguartionDetailsActivity.class);
                                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    i.putExtra("content", content);
                                    i.putExtra("configKey", configs.get(getAdapterPosition()));

                                    context.startActivity(i);

                                    dialog.cancel();
                                    //finish();
                                }
                            });

            builder.setNegativeButton(
                            "Guide Mode",
                            new DialogInterface
                                    .OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog,
                                                    int which) {

                                    Intent i = new Intent(context, CustomView.class);
                                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    i.putExtra("content", content);
                                    i.putExtra("configKey", configs.get(getAdapterPosition()));
                                    context.startActivity(i);
                                    dialog.cancel();
                                }
                            });


            // Create the Alert dialog
            AlertDialog alertDialog = builder.create();

            // Show the Alert Dialog box
            alertDialog.show();

        }
    }

    public List<String> getResults() {
        return configs;
    }
    public void setResults(List<String> configs) {
        this.configs = configs;
    }


}
