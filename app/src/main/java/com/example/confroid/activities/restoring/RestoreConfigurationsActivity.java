package com.example.confroid.activities.restoring;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.confroid.R;
import com.example.confroid.utilities.BundleTypeAdapterFactory;
import com.example.confroid.utilities.FileManager;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class RestoreConfigurationsActivity extends AppCompatActivity {

    public static final String PREFS_NAME = "ConfroidPrefsFile";
    private SharedPreferences confroidPreferences;

    private List configslist = new ArrayList();
    private List validatelist = new ArrayList();
    private Bundle configurationBundle;


    private final Gson gson = new GsonBuilder()
            .registerTypeAdapterFactory(new BundleTypeAdapterFactory())
            .create();
    ArrayAdapter<String> adapter;
    private ListView listView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restore_configurations);
        adapter=new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_multiple_choice,
                configslist);
        confroidPreferences = getSharedPreferences(PREFS_NAME, 0);
        String configs = getIntent().getStringExtra("Configs");
        listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(adapter);

        configurationBundle = gson.fromJson(configs,Bundle.class);
        for(String key: configurationBundle.keySet()){
            configslist.add(key);
        }
        adapter.notifyDataSetChanged();


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selectedItem = (String) parent.getItemAtPosition(position);
            }
        });


    }

    public void validate(View v){
        File file = new File(this.getFilesDir().getPath(),"Configuration_data");
        Bundle importBundle = gson.fromJson(FileManager.readFile(file),Bundle.class);
        SparseBooleanArray checked = listView.getCheckedItemPositions();
        int p =0;
        for (int i = 0; i < listView.getAdapter().getCount(); i++) {
            if (checked.get(i)) {
                validatelist.add(listView.getItemAtPosition(i));
            }
        }
        for(Object key: validatelist) {
            importBundle.putBundle(key.toString(),configurationBundle.getBundle(key.toString()));
        }
        FileManager.writeFile(file,gson.toJson(importBundle));
        Toast.makeText(this, " Import Succesfull", Toast.LENGTH_LONG).show();

        finish();

    }

    public void checkAll(View v){
        SparseBooleanArray checked = listView.getCheckedItemPositions();
        System.out.println(checked);
        for (int i = 0; i < listView.getAdapter().getCount(); i++) {
            checked.append(i,true);
            adapter.notifyDataSetChanged();
        }

    }

    public void unCheckall(View v){
        SparseBooleanArray checked = listView.getCheckedItemPositions();
        for (int i = 0; i < listView.getAdapter().getCount(); i++) {
            checked.append(i,false);
            adapter.notifyDataSetChanged();
        }

    }


}