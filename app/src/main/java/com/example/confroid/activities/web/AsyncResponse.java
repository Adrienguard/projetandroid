package com.example.confroid.activities.web;

public interface AsyncResponse {

    void processFinish(String output);
}
