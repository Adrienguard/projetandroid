package com.example.confroid.activities.customViews;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.confroid.R;
import com.example.confroid.activities.details.ConfiguartionDetailsActivity;
import com.example.confroid.config.Configuration;
import com.example.confroid.services.ConfigurationPusher;
import com.example.confroid.utilities.BundleTypeAdapterFactory;
import com.example.confroid.utilities.FileManager;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

public class CustomView extends AppCompatActivity {

    private List<String> versionList = new ArrayList();
    ArrayAdapter<String> adapter;
    private ListView listView;
    private Bundle configurationBundle;
    private String INIT_TAG = "0";

    public String currentVersion;
    public String configName;


    private final Gson gson = new GsonBuilder()
            .registerTypeAdapterFactory(new BundleTypeAdapterFactory())
            .create();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_view);


        adapter=new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                versionList);
        configName = getIntent().getStringExtra("configKey");
        listView = (ListView) findViewById(R.id.versionListView);

        listView.setAdapter(adapter);

        configurationBundle = gson.fromJson(getIntent().getStringExtra("content"), Bundle.class);
        for(String key: configurationBundle.keySet()){
            if(!key.equals("lastVer")) {
                versionList.add(key);
            }
        }
        adapter.notifyDataSetChanged();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                currentVersion = (String) parent.getItemAtPosition(position);

                System.out.println("iiiiiin");

                // FRAGMENT TRANSACTION TO DISPLAY FIELDS
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                ft.add(R.id.customViewConstraintLayout, ConfigurationFragment.newInstance(configurationBundle.getBundle(versionList.get(position)),INIT_TAG,"CONTENT"));
                //ft.replace(R.id.customViewLinearLayout, new ConfigurationFragment());
                ft.addToBackStack(INIT_TAG).commit();
            }
        });

    }
    @Override
    public void onBackPressed() {
        Log.i("BACK INFO", "U PRESSED BACK");
        if (getFragmentManager().getBackStackEntryCount() != 0) {
            getFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        Log.i(" INFO", "On resume");
    }

    public void saveBundle(){
        Log.i("INFO", "Bundle Saved");

        Bundle config = new Bundle();
        config.putBundle("content", configurationBundle.getBundle(currentVersion));

        config.putString("name", configName);



        // send intent to configuragion pusher with magictoken
        Intent i = new Intent();
        i.putExtra("config", config);
        i.putExtra("token","M4G1CToK3N");
        i.putExtra("tag",currentVersion);


        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            getSupportFragmentManager().beginTransaction().remove(fragment).commit();
        }
        ConfigurationPusher.enqueueWork(CustomView.this, i);
        Toast.makeText(this, "Update Config Succesfull", Toast.LENGTH_LONG).show();

    }


}