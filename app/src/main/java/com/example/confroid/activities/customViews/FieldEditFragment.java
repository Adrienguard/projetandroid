package com.example.confroid.activities.customViews;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.provider.ContactsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.confroid.R;
import com.example.confroid.config.Array;
import com.example.confroid.utilities.BundleTypeAdapterFactory;
import com.example.confroid.utilities.FileManager;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static androidx.core.content.ContextCompat.getDrawable;
import static androidx.core.content.ContextCompat.getSystemService;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FieldEditFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FieldEditFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    private static final String ARG_PARAM4 = "param4";

    // TODO: Rename and change types of parameters
    private String mParam1;  // KEY OF THE VALUE
    private Object mParam2;  // CONTENT OF THE VALUE
    private String mParam3;  // ID OF THE PARENT FRAGMENT WHICH OPENS THIS EDIT FRAGMENT
    private String mParam4;  // CLASS OF OBJECT CONTAINING THE VALUE

    //STORED VALUES
     private String savedValue;

    // gson

    private final Gson gson = new GsonBuilder()
            .registerTypeAdapterFactory(new fr.uge.confroid.lib.BundleTypeAdapterFactory())
            .create();

    //VIEWS

    EditText valueEditText;

    TextView tv;


    public FieldEditFragment() {
        // Required empty public constructor
    }

    // TEST LE TYPE DE LA VALEUR DU CHAMPS PUIS LE STOCK EN ARG 2. PUIS STOCKE TOUS LES AUTRES PARAMETRES
    public static FieldEditFragment newInstance(String param1, Object param2, String param3, String param4) {
        FieldEditFragment fragment = new FieldEditFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        if (param2 instanceof Boolean) {
            args.putBoolean(ARG_PARAM2, (boolean) param2);
        } else if (param2 instanceof Integer) {
            args.putInt(ARG_PARAM2, (int) param2);
        } else if (param2 instanceof Double) {
            args.putDouble(ARG_PARAM2, (double) param2);
        } else if (param2 instanceof String) {
            args.putString(ARG_PARAM2, (String) param2);
        } else if (param2 instanceof Float) {
            args.putFloat(ARG_PARAM2, (float) param2);
        } else if (param2 instanceof Long) {
            args.putLong(ARG_PARAM2, (long) param2);
        }
        args.putString(ARG_PARAM3, param3);


        args.putString(ARG_PARAM4, param4);

        //args.putBundle(ARG_PARAM2, null);
        fragment.setArguments(args);
        return fragment;
    }

    //ON AFFECTE LES ARGUMENTS
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            savedValue =savedInstanceState.getString("currentEdit");
        }

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1); // Key of the field
            mParam2 = getArguments().get(ARG_PARAM2);       // value of the field
            mParam3 = getArguments().getString(ARG_PARAM3); // id of parent fragment
            mParam4 = getArguments().getString(ARG_PARAM4); // class of the object containing the field
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View rootView = inflater.inflate(R.layout.fragment_field_edit, container, false);
        tv = rootView.findViewById(R.id.textViewValue);
        tv.setText(mParam2.toString());
        TextView tv2 = rootView.findViewById(R.id.textViewEntry);
        tv2.setText(mParam1);
        LinearLayout linearLayout = rootView.findViewById(R.id.editFieldLinearLayoutHorizontal);

        // Define type of view displayed based onf value type of the field
        View displayable = display();


        Button confirmButton = (Button) rootView.findViewById(R.id.updateFieldButton);
        EditText editTextkeyField = rootView.findViewById(R.id.editTextTextKeyField);


        //load item constraints
        String storagePath = "/data/user/0/fr.uge.confroid/files";
        File fileObjects = new File(storagePath, "Registered_objects");
        String registeredObjects = FileManager.readFile(fileObjects);
        Bundle registeredObjectsBundle = gson.fromJson(registeredObjects, Bundle.class);

        // set description
        setDescription(rootView, registeredObjectsBundle);
        // PhoneAnnotation

        if (registeredObjectsBundle.containsKey(mParam4)) {
            if (registeredObjectsBundle.getBundle(mParam4).containsKey(mParam1)) {
                if (registeredObjectsBundle.getBundle(mParam4).getBundle(mParam1).containsKey("PhoneNumber")) {
                    Button buttonPhoneNumber = rootView.findViewById(R.id.import_number);
                    buttonPhoneNumber.setVisibility(View.VISIBLE);
                    buttonPhoneNumber.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent contactPickerIntent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                            startActivityForResult(contactPickerIntent, 111);

                        }
                    });
                }
            }
        }

        //GEOLOCALISATION
        if (registeredObjectsBundle.containsKey(mParam4)) {
            if (registeredObjectsBundle.getBundle(mParam4).containsKey(mParam1)) {
                if (registeredObjectsBundle.getBundle(mParam4).getBundle(mParam1).containsKey("GeoCoordinates")) {
                    Button buttonGeolocalisation = rootView.findViewById(R.id.import_geolocalisation);
                    buttonGeolocalisation.setVisibility(View.VISIBLE);
                    buttonGeolocalisation.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
                            Criteria criteria = new Criteria();
                            String bestProvider = locationManager.getBestProvider(criteria, false);

                            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                // TODO: Consider calling
                                //    ActivityCompat#requestPermissions
                                // here to request the missing permissions, and then overriding
                                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                //                                          int[] grantResults)
                                // to handle the case where the user grants the permission. See the documentation
                                // for ActivityCompat#requestPermissions for more details.
                                System.out.println("J4AI  PAS LA PERMISION");
                            }
                            boolean gps_enabled = false;
                            boolean network_enabled = false;

                            gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                            network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

                            Location net_loc = null, gps_loc = null, finalLoc = null;

                            if (gps_enabled)
                                gps_loc = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (network_enabled)
                                net_loc = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                            if (gps_loc != null) {
                                finalLoc = gps_loc;
                            } else if (net_loc != null) {
                                finalLoc = net_loc;
                            }
                            /*System.out.println(finalLoc);
                            System.out.println(finalLoc.getLatitude());
                            System.out.println(finalLoc.getLongitude());*/
                            if(finalLoc != null){
                                float latitude = (float) finalLoc.getLatitude();
                                float longitude = (float) finalLoc.getLongitude();
                                float coordonees[] = new float[]{latitude,longitude};
                                valueEditText.setText(Arrays.toString(coordonees));
                            }
                        }
                    });
                }
            }
        }




        //-----------------------DOESNT ALLOW TO MODIFY CREATION DATE AND CLASS-----------------------------
        if(!mParam1.equals("class") &&  !mParam1.equals("creationDate")) {
            linearLayout.addView(displayable,4); //ADD view based on value type to layout

            confirmButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    //REGEX CONSTRAINT

                    if (!valueConstraint(registeredObjectsBundle)) {
                        Toast.makeText(getActivity(), " Error: text doesnt match constraints", Toast.LENGTH_LONG).show();
                    }
                    else {
                        Bundle newBundle = new Bundle();


                        //CONTENT
                        if (displayable instanceof CheckBox) {
                            boolean ischecked = ((CheckBox) displayable).isChecked();

                            newBundle.putString("content", String.valueOf(ischecked));
                        }
                        if (displayable instanceof EditText) {
                            newBundle.putString("content", ((EditText) displayable).getText().toString());
                        }
                        //TYPE OF CONTENT
                        BundleTypeAdapterFactory.fillBundleType(newBundle, mParam2, "type");


                        //KEY
                        if(editTextkeyField.getText().toString().equals("")){
                            newBundle.putString("key", mParam1);
                        }else{
                            newBundle.putString("key", editTextkeyField.getText().toString());
                        }

                        newBundle.putString("ancientKey", mParam1);

                        String rkey = (String.valueOf(Integer.valueOf(mParam3)));

                        // SEND RESULT TO PARENT FRAGMENT WITH MODIFIED DATA
                        getParentFragmentManager().setFragmentResult("requestKey2" + rkey, newBundle);
                        getParentFragmentManager().popBackStackImmediate();
                    }

                }


            });
        }
        else {
            confirmButton.setVisibility(View.GONE);
            editTextkeyField.setVisibility(View.GONE);
        }
        return rootView;
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putString("currentEdit",valueEditText.getText().toString());
    }

    // GET (entre autres) Phone number requested by intent sent to contacts
    // and set valueEdditex with the value
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if( requestCode == 111 ) {
            Uri contact = data.getData();
            ContentResolver contentResolver = getActivity().getContentResolver();
            Cursor contentCursor = contentResolver.query(contact, null, null,null, null);
            if(contentCursor.moveToFirst()){
                String id = contentCursor.getString(contentCursor.getColumnIndexOrThrow(ContactsContract.Contacts._ID));
                String hasPhone =
                        contentCursor.getString(contentCursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
                if (hasPhone.equalsIgnoreCase("1"))
                {
                    Cursor phones = getActivity().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = "+ id,null, null);
                    phones.moveToFirst();
                    String contactNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    Log.i("phoneNUmber", "The phone number is "+ contactNumber);
                    valueEditText.setText(contactNumber);
                }
            }
        }
    }

    // MANAGES CONSTRAIT IN POSSIBLE ANNOTATIONS (Regex, and RangeValue)
    public boolean valueConstraint(Bundle b) {
        if (b.containsKey(mParam4)) {
            if (b.getBundle(mParam4).containsKey(mParam1) && !(mParam2 instanceof String)) {
                boolean res;
                Bundle subBundle = b.getBundle(mParam4).getBundle(mParam1);
                if(subBundle.getString("RangeValidator", "infinite").equals("infinite")){
                    return true;
                }else {
                    String bornes = subBundle.getString("RangeValidator", "infinite").replace(" ","").replace("[","").replace("]","");
                    try {
                         res =  validateRange(bornes);
                    }catch (NumberFormatException e){
                        Toast.makeText(getActivity(), " Number format issue", Toast.LENGTH_LONG).show();

                        return false;
                    }
                    return res;
                }
            }
            // PATTERN MATCHING REGEX
            else if (b.getBundle(mParam4).containsKey(mParam1)) {
                Bundle subBundle = b.getBundle(mParam4).getBundle(mParam1);
                Pattern ps = Pattern.compile(b.getString("RegexValidator", ".*"));
                Matcher ms = ps.matcher(valueEditText.getText().toString());
                boolean bs = ms.matches();
                return bs;
            }

        }
        return true;
    }

    // RETURNS TRUE IF DATA is in range based on Numeric Type
    private boolean validateRange(String bornes) {
        String result = valueEditText.getText().toString();

        if(mParam2 instanceof Integer){
            int bornInf = Integer.valueOf(bornes.split(",")[0]);
            int bornSup = Integer.valueOf(bornes.split(",")[1]);
            if(bornSup>= Integer.valueOf(result)  && bornInf<=Integer.valueOf(result)){
                return true;
            }
        }else if(mParam2 instanceof Float){
            float bornInf = Float.valueOf(bornes.split(",")[0]);
            float bornSup = Float.valueOf(bornes.split(",")[1]);
            if(bornSup>= Float.valueOf(result) && bornInf<=Float.valueOf(result)){
                return true;
            }
        }else if(mParam2 instanceof Long){
            long bornInf = Long.valueOf(bornes.split(",")[0]);
            long bornSup = Long.valueOf(bornes.split(",")[1]);
            if(bornSup>= Long.valueOf(result) && bornInf<=Long.valueOf(result)){
                return true;
            }
        }else if(mParam2 instanceof Double){
            Log.d("DOUBLE INSTANCE MESSAGE","je suis un douuubleeeee");
            double bornInf = Double.valueOf(bornes.split(",")[0]);
            double bornSup = Double.valueOf(bornes.split(",")[1]);
            if(bornSup>= Double.valueOf(result) && bornInf<= Double.valueOf(result)){
                return true;
            }
        }
        return false;


    }


    // DETECTS IF FIELD IS REGISTERED WITH CONSTRAINTS and set description
    private void setDescription(View v, Bundle registeredObjectsBundle) {


        TextView description = v.findViewById(R.id.descriptionTextview);

        if(registeredObjectsBundle.containsKey(mParam4)) {
            if (registeredObjectsBundle.getBundle(mParam4).containsKey(mParam1)) {
                // DESCRIPTION
                String desc = registeredObjectsBundle.getString("Description","no Description");
                int descId = getResources().getIdentifier(mParam1, "string", getActivity().getPackageName());
                description.setText(getString(descId));
            }
        }


    }


    // SET INPUT TYPE; CHECK BOX IF BOLEAN, editext if numericType or String, TExtview for else
    private View display(){
        if(mParam2 instanceof Boolean){
            CheckBox c = new CheckBox(getContext());

            c.setChecked((boolean)mParam2);

            return c;
        }else if(mParam2 instanceof Integer|| mParam2 instanceof Double ||
                mParam2 instanceof Float || mParam2 instanceof Long || mParam2 instanceof String){
            valueEditText = new EditText(getContext());
            if (savedValue == null){valueEditText.setText(mParam2.toString(), TextView.BufferType.EDITABLE); }
            else{
                valueEditText.setText(savedValue);
            }
            valueEditText.setBackgroundColor(Color.WHITE);
            valueEditText.setBackground(getDrawable(getContext(),R.drawable.edittexstyle));
            valueEditText.setPadding(15,0,0,0);
            return valueEditText;
        }
        else return new TextView(getContext());
    }



    // J'ai preferé lfoutre cette methode dans bundletypeadapter vu qu'on la réutilise plusieures fois
    private void fillBundleType(Bundle b, Object obj){
        if(obj instanceof String){
            b.putString("type",(String)obj);
        }else if(obj instanceof Integer){
            b.putInt("type",(Integer)obj);
        }else if(obj instanceof Float){
            b.putFloat("type",(Float)obj);
        }else if(obj instanceof Double){
            b.putDouble("type",(Double)obj);
        }else if(obj instanceof Long){
            b.putLong("type",(long)obj);
        }

    }




}