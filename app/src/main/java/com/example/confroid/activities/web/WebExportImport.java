package com.example.confroid.activities.web;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.confroid.R;
import com.example.confroid.utilities.AESencryption;
import com.example.confroid.utilities.BundleTypeAdapterFactory;
import com.example.confroid.utilities.FileManager;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.IOException;
import java.security.SecureRandom;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static java.nio.charset.StandardCharsets.UTF_8;

public class WebExportImport extends AppCompatActivity implements AsyncResponse {

    KeyGenerator keyGenerator;
    SecretKey secretKey;
    byte[] IV = new byte[16];
    SecureRandom random;
    //Response

    String webResponse;

    private final Gson gson = new GsonBuilder()
            .registerTypeAdapterFactory(new BundleTypeAdapterFactory())
            .create();




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_export_import);
        Button confirmButton = (Button) findViewById(R.id.confirmWebImport);
        confirmButton.setVisibility(View.GONE);

        // INITialise clé publique
        try {
            keyGenerator = KeyGenerator.getInstance("AES");
            keyGenerator.init(256);
            secretKey = keyGenerator.generateKey();
        } catch (Exception e) {
            e.printStackTrace();
        }
        random = new SecureRandom();
        random.nextBytes(IV);

    }



    public void importWeb(View v) throws IOException {
        EditText passwordEditText = (EditText) findViewById(R.id.editTextTextPassword);
        String PASSWORD = passwordEditText.getText().toString();//"this is a password";
        DecryptMessage decryptMessage = new DecryptMessage();
        decryptMessage.execute();
    }

    @Override
    public void processFinish(String output) {
        Log.i("RESULT","result:  " + output);
    }


    private class DecryptMessage extends AsyncTask<String, Void, String> {

        EditText passwordEditText = (EditText) findViewById(R.id.editTextTextPassword);
        Button confirmButton = (Button) findViewById(R.id.confirmWebImport);
        String PASSWORD = passwordEditText.getText().toString();//"this is a password";
        String OUTPUT_FORMAT = "%-30s:%s";

        String decryptedText = null;

        public AsyncResponse delegate = null;
        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        protected String doInBackground(String... params) {
            OkHttpClient client = new OkHttpClient();
            //RequestBody body = RequestBody.create(JSON, json);
            final MediaType JSON = MediaType.get("application/json; charset=utf-8");
            String OUTPUT_FORMAT = "%-30s:%s";

            DecryptMessage task = new DecryptMessage();

            Request request = new Request.Builder()
                    .url("http://10.0.2.2:8000/storage/configurations.txt")//.url(configname.getText().toString())
                    .build();

            try {
                Response response = client.newCall(request).execute();
                decryptedText = AESencryption.decrypt(response.body().string(), PASSWORD);
                return decryptedText;
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
                return "fail";

            }

         return null;
        }

        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);
            if(result == "fail"){
                Toast.makeText(getActivity(), "Import Failed", Toast.LENGTH_LONG).show();
            }
            else {
                Log.e("ANSWER", "" + result);
                Toast.makeText(getActivity(), "import Succesfull", Toast.LENGTH_LONG).show();
                setWebResponse(result);
                confirmButton.setVisibility(View.VISIBLE);
            }

            //delegate.processFinish(result);


        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    public Activity getActivity(){
        return this;
    }

    public void setWebResponse(String webResponse){
        this.webResponse = webResponse;
    }

    public void confirmImport(View v){
        File file = new File(this.getFilesDir().getPath(),"Configuration_data");
        // ce bloc permet de ne pas supprimer les config non pressentes sur le web
        Bundle importBundle = gson.fromJson(FileManager.readFile(file),Bundle.class);
        Bundle configurationsBundle = gson.fromJson(webResponse, Bundle.class);
        for(String key: importBundle.keySet()){
            configurationsBundle.putBundle(key,importBundle.getBundle(key));
        }

        FileManager.writeFile(file,gson.toJson(configurationsBundle));



        finish();

    }



    @RequiresApi(api = Build.VERSION_CODES.O)
    public void exportWeb(View v) throws Exception {
        EditText passwordEditText = (EditText) findViewById(R.id.editTextTextPassword);

        if(!passwordEditText.getText().toString().equals("")) {
            File file = new File(this.getFilesDir().getPath(), "Configuration_data");


            String OUTPUT_FORMAT = "%-30s:%s";
            String PASSWORD = passwordEditText.getText().toString();//"this is a password";
            String pText = FileManager.readFile(file);

            String encryptedTextBase64 = AESencryption.encrypt(pText.getBytes(UTF_8), PASSWORD);

            /*System.out.println("\n------ AES GCM Password-based Encryption ------");
            System.out.println(String.format(OUTPUT_FORMAT, "Input (plain text)", pText));
            System.out.println(String.format(OUTPUT_FORMAT, "Encrypted (base64) ", encryptedTextBase64));

            System.out.println("\n------ AES GCM Password-based Decryption ------");
            System.out.println(String.format(OUTPUT_FORMAT, "Input (base64)", encryptedTextBase64));*/

            // if password not match.
            // javax.crypto.AEADBadTagException: Tag mismatch!
            //String decryptedText = EncryptorAesGcmPassword.decrypt(encryptedTextBase64, "other password");

            //Assertions.assertEquals(plainText, decryptedCipherText);

            OkHttpClient client = new OkHttpClient();

            MediaType JSON = MediaType.get("application/json; charset=utf-8");
            /*RequestBody requestBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("server", FileManager.readFile(file))
                    .build();*/
            RequestBody requestBody = RequestBody.create(JSON, encryptedTextBase64);
            Request request = new Request.Builder()
                    .url("http://10.0.2.2:8000/storage")//.url(configname.getText().toString())
                    .post(requestBody)
                    .build();

            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    e.printStackTrace();
                }

                @Override
                public void onResponse(Call call, final Response response) throws IOException {

                    if (!response.isSuccessful()) {
                        throw new IOException("Unexpected code " + response);
                    } else {
                        System.out.println(response.body().string());
                        //Toast.makeText(getActivity(), "Connection failed", Toast.LENGTH_LONG).show();
                        finish();
                    }
                }
            });
        }else {
            Toast.makeText(getActivity(), "PASSWORD CANT BE EMPTY", Toast.LENGTH_LONG).show();
        }

    }



}