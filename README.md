# Confroid

Confroid is an Android Studio project Wich stores and manages Configurations ( java Objects representing data) from client Apps. And proposes a High level api <Confroid.lib> to manipulate data Structures required.


## Usage

import project in your local AndroidProjects directory. It contains a simple Test app to test main features (save, pull configurations, request identification token and get all current configuration Versions)

```bash
git clone https://gitlab.com/Adrienguard/projetandroid.git
```

## Limitations

this is a prototype of the aiming project. theres several limitations:

- it only handle java oriented configurations
- only store object fields data
- doesnt verify intégrity of modified data in 
  classic mode edition. you have to know well the structure of your objects

- the lib imposes to know the service class names of confroid.

- Add contraints to objects using the library annotations is possible but requires to register the objects manually

```java

public class Dog {
    @Description(description = "R.string.name")
    @RegexValidator(regex = ".*")
    public String name;

    @Description(description = "R.string.age")
    @RangeValidator(range={0,25})
    public int age;
    @Description(description = "R.string.localisation")
    @GeoCoordinates
    public float[] localisationPlacecoord;
    // Constructor

}


```
````java

List objects = new ArrayList();
objects.add(new Dog());
confroid.registerObjects(Dog.this, ObjectRegistration.class,"token",objects);
````

- request a new token imposes to modify internal test app code and replace ancient token by the new
one to sumbit configs or pull a config
## Bugs
- possible que l'app crash si on modifie trop de fois d'affilé le même champs (j'ai découvert ce bug a la
toute fin et je n'ai pas reussi à en trouver l'origine)
- je n'ai pas trouvé d'autres bugs . LE reste est plutot liée à l'absence de certaines fonctionalités ou d'actions partielles


## License
[MIT](https://choosealicense.com/licenses/mit/)
